from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Member, Store, SalesRecord, Product, Ingredient, Supplier, SalesDetail, Recipe, Storage, SupplyRecord


# Register your models here.
@admin.register(Member)
class memberAdmin(ImportExportModelAdmin):
 list_display = ('id', 'name', 'gender', 'dateOfBirth', 'phone', 'contactAddress')

@admin.register(Store)
class storeAdmin(ImportExportModelAdmin):
 list_display = ('id', 'name', 'phone', 'address', 'manager')

@admin.register(SalesRecord)
class salesRecordAdmin(ImportExportModelAdmin):
 list_display = ('id', 'date', 'discount', 'store', 'member')

@admin.register(Product)
class productAdmin(ImportExportModelAdmin):
 list_display = ('id', 'name', 'price')

@admin.register(Ingredient)
class ingredientAdmin(ImportExportModelAdmin):
 list_display = ('id', 'name')

@admin.register(Supplier)
class supplierAdmin(ImportExportModelAdmin):
 list_display = ('id', 'name', 'phone', 'address', 'saler')

@admin.register(SalesDetail)
class salesDetailAdmin(ImportExportModelAdmin):
 list_display = ('id', 'salesRecord', 'product', 'amount')

@admin.register(Recipe)
class recipeAdmin(ImportExportModelAdmin):
 list_display = ('id', 'product', 'ingredient', 'amount', 'unit')

@admin.register(Storage)
class stockRecordAdmin(ImportExportModelAdmin):
 list_display = ('id', 'store', 'ingredient', 'quantity', 'unit', 'importDate')

@admin.register(SupplyRecord)
class stockRecordAdmin(ImportExportModelAdmin):
 list_display = ('id', 'supplier', 'storage', 'amount', 'unit', 'price', 'shipmentDate')