from django import forms
from django.forms.widgets import SelectDateWidget
class DateForm(forms.Form):
    beginDate = forms.DateField(widget=SelectDateWidget(years=[2018],months={10:'October',11:'November',12:"December"}))
    dueDate = forms.DateField(widget=SelectDateWidget(years=[2018],months={10:'October',11:'November',12:"December"}))
class DateFormVer2(forms.Form):
    Date = forms.DateField(widget=SelectDateWidget(years=[2018, 2019],months={1:'January' ,2:'February', \
    	3:'March', 4:'April', 5:'May', 6:'June', 7:'July', 8:'August', 9:'September', 10:'October',11:'November',12:"December"}))
class DateFormVer3(forms.Form):
    Date = forms.DateField(widget=SelectDateWidget(years=[2019],months={1:'January' }))
class DateFormVer4(forms.Form):
    searchDate = forms.DateField(widget=SelectDateWidget(years=[2018],months={10:'October',11:'November',12:"December"}))


