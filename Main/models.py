# -*- coding: utf-8 -*-

from django.db import models
#一般實體

class Member(models.Model): #會員
	#id primarykey 自已會幫你設
	name = models.CharField(max_length = 20) #名稱
	gender = models.CharField(max_length = 10, null=True, blank=True) #性別 (男、女、第三性...等)
	dateOfBirth = models.DateField(null = True, blank=True) #生日
	phone = models.CharField(max_length = 20, null=True, blank=True) #電話/手機號碼
	contactAddress = models.CharField(max_length = 100, null=True, blank=True) #住址
	#score = mmodels.IntegerField() #顧客分數(越高代表顧客貢獻度越高)

class Store(models.Model): #分店
	name = models.CharField(max_length = 20) #分店名稱
	phone = models.CharField(max_length = 20) #連絡電話/手機
	address = models.CharField(max_length = 100) #分店地址
	manager = models.CharField(max_length = 20) #分店經理姓名

class SalesRecord(models.Model): #銷售紀錄(類似訂單的概念)
	date = models.DateTimeField() #消費日期
	#tableNumber = models.IntegerField() #桌號
	discount = models.IntegerField() #折扣
	store = models.ForeignKey(Store, on_delete=models.CASCADE) #用餐分店
	member = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='salesRecord') #如果是會員就顯示該會員名稱；不是顯示非會員

class Product(models.Model): #產品
	name = models.CharField(max_length = 20) #產品名稱
	price = models.IntegerField() #產品單價

class Ingredient(models.Model): #原料(食材)
	name = models.CharField(max_length = 20) #原料名稱

class Supplier(models.Model): #供應商
	name = models.CharField(max_length = 20) #名稱
	phone = models.CharField(max_length = 20) #電話/手機號碼
	address = models.CharField(max_length = 100) #住址
	saler = models.CharField(max_length = 20) #供應商負責人

#因實體和實體的關聯產生出來的新實體

class SalesDetail(models.Model): #交易明細 (銷售紀錄 & 產品)
	salesRecord = models.ForeignKey(SalesRecord, on_delete=models.CASCADE, related_name='salesDetail') #銷售紀錄
	product = models.ForeignKey(Product, on_delete=models.CASCADE) #產品
	amount = models.IntegerField() #數量 (例如有一桌的客人點了2份美式牛肉堡，那這裡的數字就是2)

class Recipe(models.Model): #食譜 (產品 & 原料)
	product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='recipe') #產品
	ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE, related_name='recipe') #原料
	amount = models.FloatField() #所需的原料量
	unit = models.CharField(max_length = 20) #所需的原料量的單位

class Storage(models.Model): #庫存 (分店 & 原料)
	store = models.ForeignKey(Store, on_delete=models.CASCADE) #分店
	ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE, related_name='storage') #原料
	quantity = models.IntegerField() #庫存
	unit = models.CharField(max_length = 20) #庫存單位 (箱、盒)
	importDate = models.DateField() #進貨日期

class SupplyRecord(models.Model): #出貨紀錄 (庫存 & 供應商)
	supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE) # 供應商
	storage = models.ForeignKey(Storage, on_delete=models.CASCADE) # 庫存
	amount = models.IntegerField() # 出貨數量
	unit = models.CharField(max_length = 20) # 出貨單位 (箱、盒)
	price = models.IntegerField() # 出貨原料的單價
	shipmentDate = models.DateField() #出貨日期
