def handleStorage(self):
    ####得到每日庫存
    orderList = []
    #以下為件10/3初始庫存
    # myStr = "2018-10-" + str(3)
    # date0 = datetime.strptime(myStr, "%Y-%m-%d")
    # for q in range(20):
    #     ingredient = Ingredient.objects.get(pk=q+1)
    #     store = Store.objects.get(pk=1)
    #     ####現在分店隨便設，單位也是
    #     storage = Storage(ingredient = ingredient, quantity = 10, importDate = date0, store = store, unit='0')
    #     storage.save()
    for month in [10,11,12,1]:
        print(month)
        if  month == 10:
            for i in range(4,32):
                myStr = "2018-10-" + str(i)
                date0 = datetime.strptime(myStr, "%Y-%m-%d")
                date1 = date0 + timedelta(days=1)  
                username = '政大'
                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id,salesRecord__date__range=[date0, date1]).order_by('salesRecord__date')
                except Store.DoesNotExist:
                    store = None

                    detail = SalesDetail.objects.filter(salesRecord__date__range=[beginDate, dueDate1]).order_by('salesRecord__date')
                
                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()

                ingredientAmountList0 = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                    
                    added = False
                    for i in ingredientAmountList0:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList0.append([ingredientName,amounts])  

                print(ingredientAmountList0)

        ######以下是預測輛
                predictedDate=date0

                def takeZero(elem):
                    return elem[0]
                def takeOne(elem):
                    return elem[1]

                base_dir = os.path.dirname(os.path.abspath(__file__))
                db_path = os.path.join(base_dir, "../db.sqlite3")
                usedWorkingDayList = []
                aList2 = []
                startDate = predictedDate - timedelta(days=7)
                endDate = predictedDate - timedelta(days=1)

                with sqlite3.connect(db_path) as conn:
                    cur = conn.cursor()
                    cursor = cur.execute("SELECT date from main_salesrecord \
                        WHERE date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")

                    for i in cursor:
                        usedWorkingDayList.append(i[0][0:10])
                    usedWorkingDayList = set(usedWorkingDayList)
                    #print(usedWorkingDayList)

                    cursor2 = cur.execute("SELECT main_salesrecord.date, main_salesdetail.product_id, main_salesdetail.amount \
                        from main_salesrecord, main_salesdetail \
                        WHERE main_salesrecord.id = main_salesdetail.salesRecord_Id \
                        AND main_salesrecord.date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")
                    
                    for i in cursor2:
                        aList2.append([i[0],i[1],i[2]])

                usedWorkingDayNum = len(usedWorkingDayList) #前一週有幾個工作日

                for i in aList2:
                    i[0] = i[0][0:10]

                #print(aList2)

                aList2.sort(key = takeZero)
                aList2.sort(key = takeOne)

                salesVolumnOfEveryProductInUsedWorkingDateList = []
                for i in range(0,len(Product.objects.all())):
                    for j in usedWorkingDayList:
                        salesVolumnOfEveryProductInUsedWorkingDateList.append([i + 1,j , 0])

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    for j in aList2:
                        if i[0] == j[1] and i[1] == j[0]:
                            i[2] += j[2]

            # print(salesVolumnOfEveryProductInUsedWorkingDateList)

                standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList = []

                aList = []

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    aList.append(i[2])
                    if len(aList) == usedWorkingDayNum:
                    # print(aList)
                        standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList.append(np.std(aList, ddof = 1))
                        aList = []

                #print(standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList)

                #計算原料

                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id \
                        ,salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date') #取預測日前一週用的原料量
                except Store.DoesNotExist:
                    store = None
                    detail = SalesDetail.objects.all().order_by('salesRecord__date')
                    detail = SalesDetail.objects.filter(salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date')

                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()
                z = 0.95
                ingredientAmountList = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[0]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                        unit = i.unit                      
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[1]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[2]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[3]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[4]
                        predictedOrderAmount = amounts + z * std
                    
                    added = False
                    for i in ingredientAmountList:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList.append([ingredientName,predictedOrderAmount,unit]) 

                #print(ingredientAmountList)


                for i in ingredientAmountList:
                    i[1] = int(round(i[1] / usedWorkingDayNum))
            
            #[date,productname, storage, predicted]
                date = date0
                for i in range(len(ingredientAmountList)):
                    orderList.append([date, ingredientAmountList[i][0], ingredientAmountList0[i][1], ingredientAmountList[i][1]])
        elif month == 11:
            for i in range(0,30):
                myStr = "2018-11-" + str(i+1)
                date0 = datetime.strptime(myStr, "%Y-%m-%d")
                date1 = date0 + timedelta(days=1)  
                username = '政大'
                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id,salesRecord__date__range=[date0, date1]).order_by('salesRecord__date')
                except Store.DoesNotExist:
                    store = None

                    detail = SalesDetail.objects.filter(salesRecord__date__range=[beginDate, dueDate1]).order_by('salesRecord__date')
                
                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()

                ingredientAmountList0 = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                    
                    added = False
                    for i in ingredientAmountList0:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList0.append([ingredientName,amounts])  

                #print(ingredientAmountList0)

        ######以下是預測輛
                predictedDate=date0

                def takeZero(elem):
                    return elem[0]
                def takeOne(elem):
                    return elem[1]

                base_dir = os.path.dirname(os.path.abspath(__file__))
                db_path = os.path.join(base_dir, "../db.sqlite3")
                usedWorkingDayList = []
                aList2 = []
                startDate = predictedDate - timedelta(days=7)
                endDate = predictedDate - timedelta(days=1)

                with sqlite3.connect(db_path) as conn:
                    cur = conn.cursor()
                    cursor = cur.execute("SELECT date from main_salesrecord \
                        WHERE date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")

                    for i in cursor:
                        usedWorkingDayList.append(i[0][0:10])
                    usedWorkingDayList = set(usedWorkingDayList)
                    #print(usedWorkingDayList)

                    cursor2 = cur.execute("SELECT main_salesrecord.date, main_salesdetail.product_id, main_salesdetail.amount \
                        from main_salesrecord, main_salesdetail \
                        WHERE main_salesrecord.id = main_salesdetail.salesRecord_Id \
                        AND main_salesrecord.date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")
                    
                    for i in cursor2:
                        aList2.append([i[0],i[1],i[2]])

                usedWorkingDayNum = len(usedWorkingDayList) #前一週有幾個工作日

                for i in aList2:
                    i[0] = i[0][0:10]

                #print(aList2)

                aList2.sort(key = takeZero)
                aList2.sort(key = takeOne)

                salesVolumnOfEveryProductInUsedWorkingDateList = []
                for i in range(0,len(Product.objects.all())):
                    for j in usedWorkingDayList:
                        salesVolumnOfEveryProductInUsedWorkingDateList.append([i + 1,j , 0])

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    for j in aList2:
                        if i[0] == j[1] and i[1] == j[0]:
                            i[2] += j[2]

            # print(salesVolumnOfEveryProductInUsedWorkingDateList)

                standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList = []

                aList = []

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    aList.append(i[2])
                    if len(aList) == usedWorkingDayNum:
                    # print(aList)
                        standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList.append(np.std(aList, ddof = 1))
                        aList = []

                #print(standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList)

                #計算原料

                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id \
                        ,salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date') #取預測日前一週用的原料量
                except Store.DoesNotExist:
                    store = None
                    detail = SalesDetail.objects.all().order_by('salesRecord__date')
                    detail = SalesDetail.objects.filter(salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date')

                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()
                z = 0.95
                ingredientAmountList = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[0]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                        unit = i.unit                      
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[1]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[2]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[3]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[4]
                        predictedOrderAmount = amounts + z * std
                    
                    added = False
                    for i in ingredientAmountList:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList.append([ingredientName,predictedOrderAmount,unit]) 

                #print(ingredientAmountList)


                for i in ingredientAmountList:
                    i[1] = int(round(i[1] / usedWorkingDayNum))
            
            #[date,productname, storage, predicted]
                date = date0
                for i in range(len(ingredientAmountList)):
                    orderList.append([date, ingredientAmountList[i][0], ingredientAmountList0[i][1], ingredientAmountList[i][1]])    
        elif month == 12:
            for i in range(0,31):
                myStr = "2018-12-" + str(i+1)
                date0 = datetime.strptime(myStr, "%Y-%m-%d")
                date1 = date0 + timedelta(days=1)  
                username = '政大'
                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id,salesRecord__date__range=[date0, date1]).order_by('salesRecord__date')
                except Store.DoesNotExist:
                    store = None

                    detail = SalesDetail.objects.filter(salesRecord__date__range=[beginDate, dueDate1]).order_by('salesRecord__date')
                
                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()

                ingredientAmountList0 = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                    
                    added = False
                    for i in ingredientAmountList0:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList0.append([ingredientName,amounts])  

                #print(ingredientAmountList0)

        ######以下是預測輛
                predictedDate=date0

                def takeZero(elem):
                    return elem[0]
                def takeOne(elem):
                    return elem[1]

                base_dir = os.path.dirname(os.path.abspath(__file__))
                db_path = os.path.join(base_dir, "../db.sqlite3")
                usedWorkingDayList = []
                aList2 = []
                startDate = predictedDate - timedelta(days=7)
                endDate = predictedDate - timedelta(days=1)

                with sqlite3.connect(db_path) as conn:
                    cur = conn.cursor()
                    cursor = cur.execute("SELECT date from main_salesrecord \
                        WHERE date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")

                    for i in cursor:
                        usedWorkingDayList.append(i[0][0:10])
                    usedWorkingDayList = set(usedWorkingDayList)
                    #print(usedWorkingDayList)

                    cursor2 = cur.execute("SELECT main_salesrecord.date, main_salesdetail.product_id, main_salesdetail.amount \
                        from main_salesrecord, main_salesdetail \
                        WHERE main_salesrecord.id = main_salesdetail.salesRecord_Id \
                        AND main_salesrecord.date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")
                    
                    for i in cursor2:
                        aList2.append([i[0],i[1],i[2]])

                usedWorkingDayNum = len(usedWorkingDayList) #前一週有幾個工作日

                for i in aList2:
                    i[0] = i[0][0:10]

                #print(aList2)

                aList2.sort(key = takeZero)
                aList2.sort(key = takeOne)

                salesVolumnOfEveryProductInUsedWorkingDateList = []
                for i in range(0,len(Product.objects.all())):
                    for j in usedWorkingDayList:
                        salesVolumnOfEveryProductInUsedWorkingDateList.append([i + 1,j , 0])

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    for j in aList2:
                        if i[0] == j[1] and i[1] == j[0]:
                            i[2] += j[2]

            # print(salesVolumnOfEveryProductInUsedWorkingDateList)

                standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList = []

                aList = []

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    aList.append(i[2])
                    if len(aList) == usedWorkingDayNum:
                    # print(aList)
                        standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList.append(np.std(aList, ddof = 1))
                        aList = []

                #print(standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList)

                #計算原料

                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id \
                        ,salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date') #取預測日前一週用的原料量
                except Store.DoesNotExist:
                    store = None
                    detail = SalesDetail.objects.all().order_by('salesRecord__date')
                    detail = SalesDetail.objects.filter(salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date')

                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()
                z = 0.95
                ingredientAmountList = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[0]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                        unit = i.unit                      
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[1]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[2]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[3]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[4]
                        predictedOrderAmount = amounts + z * std
                    
                    added = False
                    for i in ingredientAmountList:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList.append([ingredientName,predictedOrderAmount,unit]) 

                print(ingredientAmountList)


                for i in ingredientAmountList:
                    i[1] = int(round(i[1] / usedWorkingDayNum))
            
            #[date,productname, storage, predicted]
                date = date0
                for i in range(len(ingredientAmountList)):
                    orderList.append([date, ingredientAmountList[i][0], ingredientAmountList0[i][1], ingredientAmountList[i][1]])  
        elif month == 1:
            for i in range(0,3):
                myStr = "2019-01-" + str(i+1)
                date0 = datetime.strptime(myStr, "%Y-%m-%d")
                date1 = date0 + timedelta(days=1)  
                username = '政大'
                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id,salesRecord__date__range=[date0, date1]).order_by('salesRecord__date')
                except Store.DoesNotExist:
                    store = None

                    detail = SalesDetail.objects.filter(salesRecord__date__range=[beginDate, dueDate1]).order_by('salesRecord__date')
                
                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()

                ingredientAmountList0 = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                    
                    added = False
                    for i in ingredientAmountList0:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList0.append([ingredientName,amounts])  

                #print(ingredientAmountList0)

        ######以下是預測輛
                predictedDate=date0

                def takeZero(elem):
                    return elem[0]
                def takeOne(elem):
                    return elem[1]

                base_dir = os.path.dirname(os.path.abspath(__file__))
                db_path = os.path.join(base_dir, "../db.sqlite3")
                usedWorkingDayList = []
                aList2 = []
                startDate = predictedDate - timedelta(days=7)
                endDate = predictedDate - timedelta(days=1)

                with sqlite3.connect(db_path) as conn:
                    cur = conn.cursor()
                    cursor = cur.execute("SELECT date from main_salesrecord \
                        WHERE date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")

                    for i in cursor:
                        usedWorkingDayList.append(i[0][0:10])
                    usedWorkingDayList = set(usedWorkingDayList)
                    #print(usedWorkingDayList)

                    cursor2 = cur.execute("SELECT main_salesrecord.date, main_salesdetail.product_id, main_salesdetail.amount \
                        from main_salesrecord, main_salesdetail \
                        WHERE main_salesrecord.id = main_salesdetail.salesRecord_Id \
                        AND main_salesrecord.date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")
                    
                    for i in cursor2:
                        aList2.append([i[0],i[1],i[2]])

                usedWorkingDayNum = len(usedWorkingDayList) #前一週有幾個工作日

                for i in aList2:
                    i[0] = i[0][0:10]

                #print(aList2)

                aList2.sort(key = takeZero)
                aList2.sort(key = takeOne)

                salesVolumnOfEveryProductInUsedWorkingDateList = []
                for i in range(0,len(Product.objects.all())):
                    for j in usedWorkingDayList:
                        salesVolumnOfEveryProductInUsedWorkingDateList.append([i + 1,j , 0])

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    for j in aList2:
                        if i[0] == j[1] and i[1] == j[0]:
                            i[2] += j[2]

            # print(salesVolumnOfEveryProductInUsedWorkingDateList)

                standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList = []

                aList = []

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    aList.append(i[2])
                    if len(aList) == usedWorkingDayNum:
                    # print(aList)
                        standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList.append(np.std(aList, ddof = 1))
                        aList = []

                #print(standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList)

                #計算原料

                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id \
                        ,salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date') #取預測日前一週用的原料量
                except Store.DoesNotExist:
                    store = None
                    detail = SalesDetail.objects.all().order_by('salesRecord__date')
                    detail = SalesDetail.objects.filter(salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date')

                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()
                z = 0.95
                ingredientAmountList = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[0]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                        unit = i.unit                      
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[1]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[2]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[3]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[4]
                        predictedOrderAmount = amounts + z * std
                    
                    added = False
                    for i in ingredientAmountList:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList.append([ingredientName,predictedOrderAmount,unit]) 

                #print(ingredientAmountList)


                for i in ingredientAmountList:
                    i[1] = int(round(i[1] / usedWorkingDayNum))
            
            #[date,productname, storage, predicted]
                date = date0
                for i in range(len(ingredientAmountList)):
                    orderList.append([date, ingredientAmountList[i][0], ingredientAmountList0[i][1], ingredientAmountList[i][1]])            
        print("orderList",orderList)
        print(orderList[-1])
    for i in orderList:
        i.append(0)

    #[ [2018-11-03, ingredient, 20(使用量), 30(預測量),0(訂購量)], [2018-11-04, ingredient, 30, 30,0(訂購量)], ]
    for index, i in enumerate(orderList):
        usage1 = orderList[index][2]
        date1 = orderList[index][0]
        predict2 = orderList[index+18][3]
        ingredient = Ingredient.objects.get(name=orderList[index][1])
        order = orderList[index][4]
        date0 = date1 - timedelta(days=1)  
        storage0 = Storage.objects.get(importDate=date0, ingredient=ingredient).quantity

        storage1 = storage0 - usage1 + order
        store = Store.objects.get(pk=1)
        storageModel = Storage(quantity=storage1, unit=0, importDate=date1, ingredient=ingredient, store=store)
        storageModel.save()

        if storage1 <= (predict2 * 3):
            orderList[index+18][4] = orderList[index+18][4] + (predict2 * 3) #訂貨下一期會到
        else:
            pass
            
