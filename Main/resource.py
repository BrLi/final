from import_export import resources
from .models import Member, Store, SalesRecord, Product, Ingredient, Supplier, SalesDetail, Recipe, Storage, SupplyRecord

class memberResource(resources.ModelResource):
    class Meta:
        model = Member

class storeResource(resources.ModelResource):
    class Meta:
        model = Store

class salesRecordResource(resources.ModelResource):
    class Meta:
        model = SalesRecord
		
class productResource(resources.ModelResource):
    class Meta:
        model = Product
		
class ingredientResource(resources.ModelResource):
    class Meta:
        model = Ingredient
		
class supplierResource(resources.ModelResource):
    class Meta:
        model = Supplier
		
class salesDetailResource(resources.ModelResource):
    class Meta:
        model = SalesDetail

class recipeResource(resources.ModelResource):
    class Meta:
        model = Recipe

class storageResource(resources.ModelResource):
    class Meta:
        model = Storage

class supplyRecordResource(resources.ModelResource):
    class Meta:
        model = supplyRecordResource