from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^data/store/(?P<storeId>\d+)/order/', views.get_store_orders, name='getStoreOrders'),
    url(r'^data/ingredient/(?P<ingredientId>\d+)/', views.getIngredients, name='getIngredients'),
    url(r'^statistic/product/(?P<productId>\d+)/(?P<date>\d+)', views.get_product_infos, name='getproductInfos'),
    url(r'^statistic/earnings/(?P<date>\d+)', views.getEarnings, name='getEarnings'),
    url(r'^pms/material_requirment_planning/product/(?P<productId>\d+)/date/(?P<date>\d+)', views.getMaterialRequirementPlanning, name='getmaterialRequirementPlanning'),
    url(r'^pms/schedule/(?P<date>\d+)', views.getSchedule, name='getSchedule'),
    url(r'^pms/ingredient/order/(?P<date>\d+)', views.getIngredientOrders, name='getIngredientOrders'),
    url(r'^customerAnalysis/index/(?P<date>\d+)', views.get_customer_index, name='getCustomerIndex'),
    url(r'^customerAnalysis/rfm/(?P<date>\d+)', views.getRfm, name='getRfm'),
    url(r'^accounts/login', views.login, name='login'),
    url(r'^accounts/logout', views.logout, name='logout'),
    url(r'^pms/storage', views.handleStorage, name='handleStorage'),
    url(r'^bom/$', views.BOMView.as_view()),
    # the api reference for REST framework

    # Contents
    url(r'^api/store/order/pagination/$', views.StoreOrders.as_view()),

    # Marketing
    url(r'^api/(?P<year>\d+)/(?P<month>\d+)/store/(?P<store_id>\d+)/products/$', views.BranchRevenuePerMonth.as_view()),
    url(r'^api/product/(?P<product_id>\d+)/store/(?P<store_id>\d+)/(?P<year>\d+)/month/$', views.HistorySalesPerStore.as_view()),
    url(r'^api/(?P<year>\d+)/(?P<month>\d+)/store/$', views.SalesProportionPerStore.as_view()),

    # Production
    url(r'^api/products/$', views.BOMAPIView.as_view()),

    # Storage Management
    # when-where-what
    url(r'^api/storage/(?P<storeId>\d+)/$', views.StorageStatusAPI.as_view()),
    url(r'^api/storage/view/(?P<storeId>\d+)/$', views.StorageStatusView.as_view()),

    # the development/tester page
    # url(r'^dev/(?P<storeId>\d+)/$', views.StorageStatus.as_view()),
    # url(r'^dev/all/$', views.dev_all),

]
