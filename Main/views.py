# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from .models import Member, Store, SalesRecord, Product, Ingredient, Supplier, SalesDetail, Recipe, Storage, SupplyRecord
from django.contrib import auth
from django.contrib.auth.decorators import login_required, permission_required
import random
import sqlite3
import os.path
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Sum
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from rest_framework.serializers import Serializer, ModelSerializer, StringRelatedField
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.response import Response

from django.http import JsonResponse
from .forms import *
from datetime import timedelta ,datetime
import numpy as np
from django.db.models import Sum
import math
# Create your views here.
def index(request):
    '''
    homepage/indexpage renderer
    '''
    member_list = Member.objects.all()
    product_list = Product.objects.all()
    return render(request, 'home.html', context={
        'memberList': member_list,
        'productList': product_list
    })

# 資料查詢
@login_required
def get_store_orders(request, storeId):
    '''
    get complete table of store-order-counts in the database
    '''
    username = request.user.username
    try:
        store = Store.objects.get(name=username)
        salesDetails = SalesDetail.objects.filter(salesRecord__store__name=username).order_by('salesRecord__date')
        count = SalesDetail.objects.filter(salesRecord__store__name=username).count()
    except Store.DoesNotExist:
        store = Store.objects.all()
        salesDetails = SalesDetail.objects.all()
        count = SalesDetail.objects.count()
    finally:
        paginator = Paginator(salesDetails, 10)

    try:
        sales = paginator.page(request.GET.get('page', 1))
    except PageNotAnInteger:
        sales = paginator.page(1)
    except EmptyPage:
        sales = paginator.page(paginator.num_pages)

    max_index = len(paginator.page_range)



    return render(request, 'StoreOrders.html', context={
        'store': store,
        'salesDetails': sales,
        'count': count,
        'max': max_index
    })

###這裡有用日期的表單大家可以使用
@login_required
def getIngredients(request, ingredientId):
    if request.method == 'GET':
        form  = DateForm()
        return render(request, 'Ingredients.html', {'form':form})
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            beginDate = form.cleaned_data['beginDate']
            dueDate = form.cleaned_data['dueDate']
            if beginDate > dueDate:
                return render(request, 'Ingredients.html', context={
                'form':form,'error':'begin date需早於due date'})
            dueDate1 = dueDate + timedelta(days=1)

            username = request.user.username
            try:
                store = Store.objects.get(name=username)
                detail = SalesDetail.objects.filter(salesRecord__store__id=store.id,salesRecord__date__range=[beginDate, dueDate1]).order_by('salesRecord__date')
            except Store.DoesNotExist:
                store = None

                detail = SalesDetail.objects.filter(salesRecord__date__range=[beginDate, dueDate1]).order_by('salesRecord__date')

            detail1 = detail.filter(product_id=1).count()#總匯三明治
            detail2 = detail.filter(product_id=2).count()#牛肉起司堡
            detail3 = detail.filter(product_id=3).count()#大亨堡
            detail4 = detail.filter(product_id=4).count()#薯條
            detail5 = detail.filter(product_id=5).count()#鮮奶茶

            recipeList = Recipe.objects.all()

            ingredientAmountList = []
            for i in recipeList:
                if i.product.id == 1:
                    amounts = i.amount * detail1
                    ingredientName = i.ingredient.name
                elif i.product.id == 2:
                    amounts = i.amount * detail2
                    ingredientName = i.ingredient.name
                elif i.product.id == 3:
                    amounts = i.amount * detail3
                    ingredientName = i.ingredient.name
                elif i.product.id == 4:
                    amounts = i.amount * detail4
                    ingredientName = i.ingredient.name
                elif i.product.id == 5:
                    amounts = i.amount * detail5
                    ingredientName = i.ingredient.name

                added = False
                for i in ingredientAmountList:
                    if  ingredientName == i[0]:
                        i[1] += amounts
                        added = True
                        break
                    else:
                        pass
                if not added:
                    ingredientAmountList.append([ingredientName,amounts])

            form  = DateForm()
            return render(request, 'Ingredients.html', context={
                'ingredientAmountList': ingredientAmountList,
                'beginDate': beginDate,
                'form':form,
                'dueDate':dueDate,
                'store':store})
        else:
            return render(request, 'Ingredients.html', context={
                'form':form,})


# 銷售統計
@login_required
def get_product_infos(request, productId, date):
    if request.method =='GET':
        product = Product.objects.all()
        return render(request, 'ProductInfos.html', context={
            'products': product,
            'dates': [10,11,12],
            })
    if request.method == 'POST':
        productId = request.POST.get('productId')
        date = request.POST.get('date')
        product = Product.objects.get(pk=productId)
        products = Product.objects.all()

        ##日期的地方可以是這換換看起點終點
        productInfos = SalesDetail.objects.filter(salesRecord__date__range=["2018-"+date+"-01", "2018-"+date+"-30"], product_id=productId).order_by('salesRecord__date')

        salesVolumnDict = SalesDetail.objects.filter(salesRecord__date__range=["2018-"+date+"-01", "2018-"+date+"-30"], product_id=productId).order_by('salesRecord__date').aggregate(Sum('amount'))

        salesVolumn = salesVolumnDict.get('amount__sum') * Product.objects.filter(id=productId).aggregate(Sum('price')).get('price__sum')
        salesVolumnInfo = str(product.name) + " " + str(date) + "月銷售額：" +  str(salesVolumn) + "元"

        return render(request, 'ProductInfos.html', context={
            'productInfos': productInfos,
            'product': product,
            'date': date,
            #'graph': "未知",
            'products': products,
            'dates': [10,11,12],
            'salesVolumnInfo' : salesVolumnInfo
        })

@login_required
def getEarnings(request, date):
    if request.method =='GET':
        product = Product.objects.all()
        return render(request, 'Earnings.html', context={
            'dates': [10,11,12],
            })
    if request.method == 'POST':
        ##營業額月份統計
        date = request.POST.get('date')
        products = Product.objects.all()
        productTotalEaringList = []
        for i in products:
            salesDetailSingleProduct = SalesDetail.objects.filter(salesRecord__date__range=["2018-"+date+"-01", "2018-"+date+"-30"], product_id=i.id).order_by('salesRecord__date')
            totalEarningFromOneProduct = 0
            for q in salesDetailSingleProduct:
                totalEarningFromOneProduct += q.amount * i.price
            productTotalEaringList.append([i.name, totalEarningFromOneProduct])
        totalEarnings = sum(map(lambda x: x[1], productTotalEaringList))
        print(totalEarnings)
        return render(request, 'Earnings.html', context={
            'productTotalEarningList': productTotalEaringList,
            'date': date,
            'dates': [10,11,12],
            'totalEarnings': totalEarnings,
        })

@login_required
def handleStorage(request):
    if request.method == 'GET':
        form  = DateFormVer4()
        return render(request, 'Storage.html', {'form':form})
    if request.method == 'POST':
        try:
            form = DateFormVer4(request.POST)
            if form.is_valid():
                date = form.cleaned_data['searchDate']
                date1 = date + timedelta(days=1)

                storages = Storage.objects.filter(importDate=date)
                storages1 = Storage.objects.filter(importDate=date1)

                #得到該日銷售量
                # detail = SalesDetail.objects.filter(salesRecord__date__range=[date, date1]).order_by('salesRecord__date')

                # detail1 = detail.filter(product_id=1).count()#總匯三明治
                # detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                # detail3 = detail.filter(product_id=3).count()#大亨堡
                # detail4 = detail.filter(product_id=4).count()#薯條
                # detail5 = detail.filter(product_id=5).count()#鮮奶茶

                # recipeList = Recipe.objects.all()

                # ingredientAmountList = []
                # for i in recipeList:
                #     if i.product.id == 1:
                #         amounts = i.amount * detail1
                #         ingredientName = i.ingredient.name
                #     elif i.product.id == 2:
                #         amounts = i.amount * detail2
                #         ingredientName = i.ingredient.name
                #     elif i.product.id == 3:
                #         amounts = i.amount * detail3
                #         ingredientName = i.ingredient.name
                #     elif i.product.id == 4:
                #         amounts = i.amount * detail4
                #         ingredientName = i.ingredient.name
                #     elif i.product.id == 5:
                #         amounts = i.amount * detail5
                #         ingredientName = i.ingredient.name

                #     added = False
                #     for i in ingredientAmountList:
                #         if  ingredientName == i[0]:
                #             i[1] += amounts
                #             added = True
                #             break
                #         else:
                #             pass
                #     if not added:
                #         ingredientAmountList.append([ingredientName,amounts])


                storageList = []
                for i in storages:
                    orderMinus = Storage.objects.get(importDate=date1, ingredient=i.ingredient).quantity-i.quantity
                    #for q in ingredientAmountList:
                        # if i.ingredient.name == q[0]:
                        #     print('add')
                        #     orderMinus += q[1]
                    if  orderMinus > 0:
                        message = '需要訂貨: ' + str(orderMinus)
                    else:
                        message = '不須訂貨'

                    storageList.append([i.ingredient.name, i.quantity, message])


                form  = DateFormVer4()
                dateInfo = "日期：" + str(date)
                return render(request, 'Storage.html', context={
                    'storages': storageList,
                    'date' : date,
                    'dateInfo' : dateInfo,
                    'form' : form})

        except(IndexError):
            return render(request, 'Storage.html', {'form':form,
            'errorMessage':'目前開放查詢日期為2018/10/4~2018/12/31'})

# 作業管理

@login_required
def getMaterialRequirementPlanning(request, productId, date):

    return render(request, 'MaterialRequirementPlanning.html', context={
        'product': '牛肉堡',
        'date': '01/02',
        'data': [['09:00', 10], ['10:00', 20], ['11:00', 30]]
    })

@login_required
def getSchedule(request, date):

    if request.method == 'GET':
        form  = DateFormVer3()
        return render(request, 'Schedule.html', {'form':form})

    if request.method == 'POST':
        try:
            form = DateFormVer3(request.POST)
            if form.is_valid():
                date = form.cleaned_data['Date']
                employeeList = [['員工A'], ['員工B'], ['員工C']]

                period = ['員工 \\ 時段','6:00','6:15','6:30','6:45','7:00','7:15','7:30','7:45']

                employeeList[0].append('洗菜')
                employeeList[0].append('洗菜')
                employeeList[0].append('切菜')
                employeeList[0].append('切菜')

                if (int(str(date)[8:10]) + 1) // 2 == 1:
                    employeeList[0].append('切菜')

                employeeList[1].append('')
                employeeList[1].append('肉品解凍')
                employeeList[1].append('')
                employeeList[1].append('')
                employeeList[1].append('製造絞肉')
                employeeList[1].append('製作肉排')

                if (int(str(date)[8:10]) + 1) // 2 == 1:
                    employeeList[1].append('製作肉排')

                if (int(str(date)[8:10]) + 1) // 2 == 1:
                    employeeList[2].append('')
                    employeeList[2].append('')
                    employeeList[2].append('')
                    employeeList[2].append('')
                    employeeList[2].append('製造絞肉')
                    employeeList[2].append('製作肉排')
                    employeeList[2].append('製作肉排')

                if (int(str(date)[8:10])) > 4:
                    employeeList = [['員工A'], ['員工B'], ['員工C']]

                print(employeeList)
                form  = DateFormVer3()
                dateInfo = "日期：" + str(date)
                return render(request, 'Schedule.html', context={
                    'employeeList': employeeList,
                    'period': period,
                    'date' : date,
                    'dateInfo' : dateInfo,
                    'form' : form})

        except(IndexError):
            return render(request, 'Schedule.html', {'form':form,
            'errorMessage':'目前開放查詢日期為2018/10/3~2019/1/3'})

@login_required
def getIngredientOrders(request, date):
    if request.method == 'GET':
        form  = DateFormVer2()
        return render(request, 'IngredientOrder.html', {'form':form})
    if request.method == 'POST':
        try:
            form = DateFormVer2(request.POST)
            if form.is_valid():
                predictedDate = form.cleaned_data['Date']

                username = request.user.username

                #取每個產品的標準差
                #資料庫的絕對路徑

                def takeZero(elem):
                    return elem[0]
                def takeOne(elem):
                    return elem[1]

                base_dir = os.path.dirname(os.path.abspath(__file__))
                db_path = os.path.join(base_dir, "../db.sqlite3")
                usedWorkingDayList = []
                aList2 = []
                startDate = predictedDate - timedelta(days=7)
                endDate = predictedDate - timedelta(days=1)

                with sqlite3.connect(db_path) as conn:
                    cur = conn.cursor()
                    cursor = cur.execute("SELECT date from main_salesrecord \
                        WHERE date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")

                    for i in cursor:
                        usedWorkingDayList.append(i[0][0:10])
                    usedWorkingDayList = set(usedWorkingDayList)
                    print(usedWorkingDayList)

                    cursor2 = cur.execute("SELECT main_salesrecord.date, main_salesdetail.product_id, main_salesdetail.amount \
                        from main_salesrecord, main_salesdetail \
                        WHERE main_salesrecord.id = main_salesdetail.salesRecord_Id \
                        AND main_salesrecord.date BETWEEN '" + str(startDate) + " 00:00:00' AND '" + str(endDate) + " 23:59:59';")

                    for i in cursor2:
                        aList2.append([i[0],i[1],i[2]])

                usedWorkingDayNum = len(usedWorkingDayList) #前一週有幾個工作日

                for i in aList2:
                    i[0] = i[0][0:10]

                print(aList2)

                aList2.sort(key = takeZero)
                aList2.sort(key = takeOne)

                salesVolumnOfEveryProductInUsedWorkingDateList = []
                for i in range(0,len(Product.objects.all())):
                    for j in usedWorkingDayList:
                        salesVolumnOfEveryProductInUsedWorkingDateList.append([i + 1,j , 0])

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    for j in aList2:
                        if i[0] == j[1] and i[1] == j[0]:
                            i[2] += j[2]

                print(salesVolumnOfEveryProductInUsedWorkingDateList)

                standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList = []

                aList = []

                for i in salesVolumnOfEveryProductInUsedWorkingDateList:
                    aList.append(i[2])
                    if len(aList) == usedWorkingDayNum:
                        print(aList)
                        standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList.append(np.std(aList, ddof = 1))
                        aList = []

                print(standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList)

                #計算原料

                try:
                    store = Store.objects.get(name=username)
                    detail = SalesDetail.objects.filter(salesRecord__store__id=store.id \
                        ,salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date') #取預測日前一週用的原料量
                except Store.DoesNotExist:
                    store = None
                    detail = SalesDetail.objects.all().order_by('salesRecord__date')
                    detail = SalesDetail.objects.filter(salesRecord__date__range=[predictedDate - timedelta(days=7) , \
                        predictedDate - timedelta(days=1)]).order_by('salesRecord__date')

                detail1 = detail.filter(product_id=1).count()#總匯三明治
                detail2 = detail.filter(product_id=2).count()#牛肉起司堡
                detail3 = detail.filter(product_id=3).count()#大亨堡
                detail4 = detail.filter(product_id=4).count()#薯條
                detail5 = detail.filter(product_id=5).count()#鮮奶茶

                recipeList = Recipe.objects.all()
                z = 0.95
                ingredientAmountList = []
                for i in recipeList:
                    if i.product.id == 1:
                        amounts = i.amount * detail1
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[0]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 2:
                        amounts = i.amount * detail2
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[1]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 3:
                        amounts = i.amount * detail3
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[2]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 4:
                        amounts = i.amount * detail4
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[3]
                        predictedOrderAmount = amounts + z * std
                    elif i.product.id == 5:
                        amounts = i.amount * detail5
                        ingredientName = i.ingredient.name
                        unit = i.unit
                        std = i.amount * standardDeviationOfSalesVolumnOfEveryProductInUsedWorkingDateList[4]
                        predictedOrderAmount = amounts + z * std

                    added = False
                    for i in ingredientAmountList:
                        if  ingredientName == i[0]:
                            i[1] += amounts
                            added = True
                            break
                        else:
                            pass
                    if not added:
                        ingredientAmountList.append([ingredientName,predictedOrderAmount,unit])

                print(ingredientAmountList)


                for i in ingredientAmountList:
                    i[1] = int(math.ceil(i[1] / usedWorkingDayNum))


                form  = DateFormVer2()



                return render(request, 'IngredientOrder.html', context={
                    'ingredientAmountList': ingredientAmountList,
                    'beginDate': predictedDate,
                    'form':form,
                    'store':store})
        except(IndexError):
            return render(request, 'IngredientOrder.html', {'form':form,
            'errorMessage':'目前開放查詢日期為2018/10/3~2019/1/3'})
        else:
            return render(request, 'IngredientOrder.html', context={
                'form':form,})

# 顧客行銷
@login_required
def get_customer_index(request, date):
    return render(request, 'CustomerIndex.html', context={
        'date': "01/02",
        'text': [['存活率', 0.8], ['ㄚㄚ綠', 0.7]]
    })

@login_required
def getRfm(request, date):
    if request.method =='GET':
        return render(request, 'Rfm.html', context={
            'year': [2018],
            'month': [10,11,12],
            'rfm_recencyWeight' : [1,2,3,4,5],
            'rfm_frequencyWeight' : [1,2,3,4,5],
            'rfm_monetaryValueWeight' : [1,2,3,4,5]
        })

    if request.method == 'POST':
        #取得使用者的輸入
        year = request.POST.get('year')
        month = request.POST.get('month')
        rfm_recencyWeight = int(request.POST.get('rfm_recencyWeight'))
        rfm_frequencyWeight = int(request.POST.get('rfm_frequencyWeight'))
        rfm_monetaryValueWeight = int(request.POST.get('rfm_monetaryValueWeight'))

        rfm_recencyList = []
        rfm_frequencyList = []
        rfm_monetaryValueList = []

        #資料庫的絕對路徑
        base_dir = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(base_dir, "../db.sqlite3")

        #SQL

        with sqlite3.connect(db_path) as conn:
            cur = conn.cursor()

            """
            cursor = cur.execute("SELECT main_member.name , main_salesrecord.date, \
                main_salesdetail.product_id, SUM(main_salesdetail.amount) from main_salesrecord, main_salesdetail, main_member \
                WHERE main_member.id != 1 AND date BETWEEN '2018-10-01 00:00:00' AND '2018-10-31 23:59:59' \
                AND main_member.id = main_salesrecord.member_id AND main_salesrecord.id = main_salesdetail.salesRecord_id \
                GROUP BY main_member.name, main_salesdetail.product_id;")
            for i in cursor:
                aList.append([i[0], i[1], i[2], i[3]]) #會員名稱 消費日期 產品 數量
            """

            memberList = []
            cursor_member = cur.execute("SELECT main_member.name \
                from main_member, main_salesrecord \
                WHERE main_member.id != 1 \
                AND main_salesrecord.date BETWEEN '" + str(year) + "-" + str(month) \
                + "-01 00:00:00' AND '" + str(year) + "-" + str(month) + "-31 23:59:59' \
                AND main_member.id = main_salesrecord.member_id \
                GROUP BY main_member.name;")

            for i in cursor_member:
                memberList.append(i[0]) #會員名稱

            memberNum = len(memberList)
            #print(memberNum)

            #R
            cursor_recency= cur.execute("SELECT main_member.name , main_salesrecord.date \
                from main_member, main_salesrecord \
                WHERE main_member.id != 1 \
                AND main_salesrecord.date BETWEEN '" + str(year) + "-" + str(month) \
                + "-01 00:00:00' AND '" + str(year) + "-" + str(month) + "-31 23:59:59' \
                AND main_member.id = main_salesrecord.member_id \
                ORDER BY main_salesrecord.date DESC;")

            aList = []
            rfm_recencyGroupNum = 5
            groupRange = memberNum // rfm_recencyGroupNum
            count = 1

            for i in cursor_recency:
                if i[0] not in aList:
                    rank = count // groupRange + 1
                    if rank > rfm_recencyGroupNum:
                        rank = rfm_recencyGroupNum
                    aList.append(i[0])
                    rfm_recencyList.append([i[0],i[1],rank])
                    count += 1

            #F
            cursor_frequency = cur.execute("SELECT main_member.name , SUM(main_salesdetail.amount / main_salesdetail.amount) \
                from main_product, main_salesrecord, main_salesdetail, main_member \
                WHERE main_member.id != 1 \
                AND main_salesrecord.date BETWEEN '" + str(year) + "-" + str(month) \
                + "-01 00:00:00' AND '" + str(year) + "-" + str(month) + "-31 23:59:59' \
                AND main_product.id = main_salesdetail.product_id \
                AND main_member.id = main_salesrecord.member_id AND main_salesrecord.id = main_salesdetail.salesRecord_id \
                GROUP BY main_member.name \
                ORDER BY SUM(main_salesdetail.amount / main_salesdetail.amount) DESC;")

            rfm_frequencyGroupNum = 5
            groupRange = memberNum // rfm_frequencyGroupNum
            count = 1

            for i in cursor_frequency:
                rank = count // groupRange + 1
                if rank > rfm_frequencyGroupNum:
                    rank = rfm_frequencyGroupNum
                rfm_frequencyList.append([i[0], i[1], rank])
                count += 1

            #M
            cursor_totalMonetaryValue = cur.execute("SELECT main_member.name , SUM(main_salesdetail.amount * main_product.price) \
                from main_product, main_salesrecord, main_salesdetail, main_member \
                WHERE main_member.id != 1 \
                AND main_salesrecord.date BETWEEN '" + str(year) + "-" + str(month) \
                + "-01 00:00:00' AND '" + str(year) + "-" + str(month) + "-31 23:59:59' \
                AND main_product.id = main_salesdetail.product_id \
                AND main_member.id = main_salesrecord.member_id AND main_salesrecord.id = main_salesdetail.salesRecord_id \
                GROUP BY main_member.name\
                ORDER BY SUM(main_salesdetail.amount) DESC;")


            aList = []
            count = 1

            for i in cursor_totalMonetaryValue:
                rfm_monetaryValueList.append([i[0], i[1] / rfm_frequencyList[count - 1][1]])
                count += 1

            for i in cursor_totalMonetaryValue:
                aList.append([i[0], i[1]])

            # 获取列表的第二个元素
            def takeSecond(elem):
                return elem[1]

            # 指定第二个元素排序
            rfm_monetaryValueList.sort(key = takeSecond, reverse = True)

            rfm_monetaryValueGroupNum = 5
            groupRange = memberNum // rfm_monetaryValueGroupNum
            count = 1

            for i in rfm_monetaryValueList:
                rank = count // groupRange + 1
                if rank > rfm_monetaryValueGroupNum:
                    rank = rfm_monetaryValueGroupNum
                i.append(rank)
                count += 1

        #print(rfm_recencyList) #顧客名稱, 本月最近一次的購買時間, 評級 (1最好；5最差)

        #print(rfm_frequencyList) #顧客名稱, 本月消費頻率, 評級 (1最好；5最差)

        #print(rfm_monetaryValueList) #顧客名稱, 本月每次消費平均花費, 評級 (1最好；5最差)

        def takeZero(elem):
            return elem[0]

        # 指定第一个元素排序
        rfm_recencyList.sort(key = takeZero)
        rfm_frequencyList.sort(key = takeZero)
        rfm_monetaryValueList.sort(key = takeZero)

        #print(rfm_recencyList, rfm_frequencyList, rfm_monetaryValueList)


        rfmList = []

        for i in range(0,memberNum):
            rfmList.append([rfm_recencyList[i][0], rfm_recencyList[i][2], rfm_frequencyList[i][2], rfm_monetaryValueList[i][2]])

        scoreBaseList = [5,4,3,2,1]

        for i in rfmList:
            i.append(scoreBaseList[i[1] - 1] * rfm_recencyWeight + scoreBaseList[i[2] - 1] * rfm_frequencyWeight \
             + scoreBaseList[i[3] - 1] * rfm_monetaryValueWeight)


        def takeFour(elem):
            return elem[4]

        rfmList.sort(key = takeFour, reverse = True)

        print(rfmList)

        return render(request, 'Rfm.html', context={
            'year' : [2018],
            'month' : [10,11,12],
            'rfm_recencyWeight' : [1,2,3,4,5],
            'rfm_frequencyWeight' : [1,2,3,4,5],
            'rfm_monetaryValueWeight' : [1,2,3,4,5],
            'rfmList': rfmList
        })


def login(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect(reverse('index'))
        else:
            return render(request, 'Login.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(request, username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            else:
                return redirect(reverse('index'))
        else:
            return render(request, 'Login.html', context={
                'errorMessage': '密碼錯誤！',
            })

def logout(request):
    auth.logout(request)
    return redirect(reverse('index'))

def dev(request, store_id):
    store = Store.objects.get(id=store_id)
    return render(request, "dev.html", context={'store': store})

def dev_all(request):
    return render(request, "dev-stacked-bar-chart.html")

class SalesDetailSerializer(ModelSerializer):
    class Meta:
        model = SalesDetail
        fields = ('product', 'amount')


class MemberSerializer(ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'name')


class SalesRecordSerializer(ModelSerializer):
    salesDetail = SalesDetailSerializer(many=True, read_only=True)
    member = MemberSerializer(read_only=True)
    class Meta:
        model = SalesRecord
        fields = ('date', 'discount', 'store', 'member', 'salesDetail')


class StoreOrders(GenericAPIView):
    queryset = SalesRecord.objects.all()
    serializer_class = SalesRecordSerializer

    def get(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = SalesRecordSerializer(queryset, many=True)
        return Response(serializer.data)


class ChartData(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    def get(self, request, storeId, month):
        product = Product.objects.all()
        labels = product.values_list('name')
        detail = SalesDetail.objects.values().filter(salesRecord__store__id=storeId)
        detail1 = detail.filter(product_id=1).count()
        detail2 = detail.filter(product_id=2).count()
        detail3 = detail.filter(product_id=3).count()
        detail4 = detail.filter(product_id=4).count()
        detail5 = detail.filter(product_id=5).count()
        data = {
            'labels': labels,
            'detail': [detail1, detail2, detail3, detail4, detail5]
        }
        return Response(data)
class BranchRevenuePerMonth(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    '''
    GET method of branch_revenue_per_month API:
    returns data includes product item names and their corresponded amount of sold in JSON form
    '''
    def get(self, request, year, month, store_id):
        # get distinct products' name
        product = Product.objects.all()
        # X-axis is product list
        labels = product.values_list('name')
        # get sales details
        start_date = datetime.date(int(year), int(month), 1)
        end_date = datetime.date(int(year), int(month)+1, 1)
        detail = SalesDetail.objects.values().filter(salesRecord__store__id=store_id)
        detail = detail.filter(salesRecord__date__range=(start_date, end_date))
        detail1 = detail.filter(product_id=1).count()
        detail2 = detail.filter(product_id=2).count()
        detail3 = detail.filter(product_id=3).count()
        detail4 = detail.filter(product_id=4).count()
        detail5 = detail.filter(product_id=5).count()
        data = {
            'labels': labels,
            'detail': [detail1, detail2, detail3, detail4, detail5]
        }
        return Response(data)

class HistorySalesPerStore(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    '''
    GET method of history_sales_per_store API:
    returns data includes product item names and their corresponded amount of sold in JSON form
    '''
    def get(self, request, product_id, store_id, year):
        # X-axis is months
        labels = ["十月", "十一月", "十二月"]
        # get sales details
        detail = SalesDetail.objects.values().filter(product_id=product_id)
        detail = detail.filter(salesRecord__store__id=store_id)
        detail = detail.filter(salesRecord__date__year=year)
        detail1 = detail.filter(salesRecord__date__month=10).count()
        detail2 = detail.filter(salesRecord__date__month=11).count()
        detail3 = detail.filter(salesRecord__date__month=12).count()
        data = {
            'labels': labels,
            'detail': [detail1, detail2, detail3]
        }
        return Response(data)


class SalesProportionPerStore(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    '''
    GET method of sales_propotion_per_store API:
    returns data includes product item names and their corresponded amount of sold in JSON form
    (use horizental stacked bar chart)
    '''
    def get(self, request, year, month):
        # get distinct products' name
        branch = Store.objects.values_list("name", flat=True)
        # X-axis is # of sales
        # get sales details
        detail = [[0 for _ in range(5)] for _ in range(3)]
        for b_s in range(0, 3):
            for item in range(0, 5):
                d = SalesRecord.objects.filter(store_id=(b_s + 1))
                d = d.filter(salesDetail__product_id=(item + 1))
                d = d.aggregate(Sum('salesDetail__amount'))
                detail[b_s][item] = d.get('salesDetail__amount__sum')

        data = {
            'labels': branch,
            # details should be in form of [{a, b, c, d, e}, {v, w, x, y, z}, {1, 2, 3, 4 ,5}]
            'detail': detail
        }
        return Response(data)

# BOM

class IngredientSerializer(ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('name',)

class RecipeSerializer(ModelSerializer):
    ingredient = IngredientSerializer(read_only=True)
    class Meta:
        model = Recipe
        fields = ('ingredient', 'amount', 'unit')

class ProductSerializer(ModelSerializer):
    recipe = RecipeSerializer(many=True, read_only=True)
    class Meta:
        model = Product
        fields = ('name', 'recipe')

class BOMView(ListAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    renderer_classes = [TemplateHTMLRenderer,]
    def get(self, request):
        queryset = Product.objects.all()
        serializer = ProductSerializer(queryset, many=True)
        return Response({'product': serializer.data},
                        template_name='bom.html')


class BOMAPIView(ListAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Product.objects.all()
    serializer_class = ProductSerializer


# Basic MRP System View

class StoreSerializer(ModelSerializer):
    class Meta:
        model = Store
        fields = ('name', )

class StorageSerializer(ModelSerializer):
    ingredient = IngredientSerializer(read_only=True)
    store = StoreSerializer(read_only=True)
    class Meta:
        model = Storage
        fields = ('ingredient', 'quantity', 'unit', 'importDate', 'store')


class StorageStatusAPI(ListAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Storage.objects.all()
    serializer_class = StorageSerializer


class StorageStatus(ListAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Storage.objects.all()
    serializer_class = StorageSerializer
    renderer_classes = [TemplateHTMLRenderer,]
    def get(self, request, storeId):
        queryset = Storage.objects.filter(store=storeId)
        serializer = StorageSerializer(queryset, many=True)
        return Response({'data': serializer.data},
                        template_name='test-template.html')


class StorageStatusView(APIView):
    def get(self, request, storeId):

        storage = Storage.objects.filter(store=storeId)
        labels = storage.values_list('importDate')

        detail = storage.values()


        data = {
            'labels': labels,
            'detail': detail
        }
        return Response(detail)
