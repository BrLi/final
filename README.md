a simple django project

- check for LICENSE first

- helper command to collect static files

```(bash)
$ python manage.py collectstatic
```
- useful configurations in `config` folder

- combined with letsencrypt/certbot and nginx
